#!/usr/bin/env python

from flask.ext.testing import TestCase

from app import create_app, db
from . import add_test_data


class BaseTest(TestCase):
    def create_app(self):
        app = create_app('TestingConfig')
        return app

    def setUp(self):
        db.create_all()
        add_test_data()

    def tearDown(self):
        db.session.remove()
        db.drop_all()
