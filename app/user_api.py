#!/usr/bin/env python

from flask import Blueprint, jsonify, request
from flask.ext.jwt import current_identity, jwt_required

user_api = Blueprint('user_api', __name__)


@user_api.route('/user/update_password', methods=['POST'])
@jwt_required()
def reset_password():
    if request.json and 'password' in request.json:
        try:
            current_identity.update_password(request.json['password'])
        except AssertionError as ae:
            return jsonify(message=ae.message), 406
        return ''
    else:
        return jsonify(message='Not JSON request with password'), 400