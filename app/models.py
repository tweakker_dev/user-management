#!/usr/bin/env python

from datetime import datetime
from sqlalchemy import UniqueConstraint
from sqlalchemy.types import VARBINARY
from sqlalchemy_utils.types.password import PasswordType

from .database import db


class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(255), unique=True, nullable=False)
    password = db.Column(PasswordType(schemes=['bcrypt'], max_length=255), nullable=False)
    roles = db.relationship('Role', secondary='user_roles', backref='users')
    password_timestamp = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    password_reset = db.Column(db.Boolean, nullable=False, default=True)
    login_attempts = db.Column(db.Integer, nullable=False, default=0)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    updated_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __repr__(self):
        return self.username

    def get_role_names(self):
        return [role.name for role in self.roles]

    def add_login_attempt(self):
        self.login_attempts += 1
        db.session.commit()

    def reset_login_attempts(self):
        self.login_attempts = 0
        db.session.commit()

    def update_password(self, new_password):
        self.password = new_password
        db.session.commit()


class PasswordHistory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='cascade'), nullable=False)
    user = db.relationship('User')
    password = db.Column(VARBINARY(length=255), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False)


class Role(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(255), unique=True, nullable=False)

    def __repr__(self):
        return self.name


class UserRoles(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id', ondelete='cascade'), nullable=False)
    user = db.relationship('User')
    role_id = db.Column(db.Integer, db.ForeignKey('role.id', ondelete='cascade'), nullable=False)
    role = db.relationship('Role')

    UniqueConstraint(user_id, role_id, name='user_roles_unique_index')
