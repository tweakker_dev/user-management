#!/usr/bin/env python

from flask.ext.admin import Admin
from flask.ext.admin.contrib.sqla import ModelView
from flask.ext.admin.form import rules
from wtforms.fields import PasswordField

from .database import db
from .models import User, PasswordHistory, Role, UserRoles


class BaseModelView(ModelView):
    def __init__(self, model, **kwargs):
        super(BaseModelView, self).__init__(model, db.session, **kwargs)


class UserView(BaseModelView):
    form_extra_fields = {'new_password': PasswordField('New password')}
    form_overrides = dict(password=PasswordField)

    form_create_rules = [rules.FieldSet(['username', 'password', 'roles', 'password_reset'])]
    form_edit_rules = [rules.FieldSet(['username', 'new_password', 'roles', 'password_reset', 'login_attempts'])]

    column_list = ('username', 'roles', 'password_reset', 'login_attempts',
                   'created_at', 'updated_at', 'password_timestamp')

    column_labels = dict(
        password_reset     = 'Password reset forced',
        login_attempts     = 'Failed login attempts',
        created_at         = 'Created at',
        updated_at         = 'Updated at',
        password_timestamp = 'Password timestamp'
    )

    column_descriptions = dict(
        created_at         = 'UTC time',
        updated_at         = 'UTC time',
        password_timestamp = 'UTC time'
    )

    column_formatters = dict(
        password           = lambda v, c, m, p: m.password.hash,
        created_at         = lambda v, c, m, p: m.created_at.strftime('%Y-%m-%d %H:%M:%S'),
        updated_at         = lambda v, c, m, p: m.updated_at.strftime('%Y-%m-%d %H:%M:%S'),
        password_timestamp = lambda v, c, m, p: m.password_timestamp.strftime('%Y-%m-%d %H:%M:%S')
    )

    def on_model_change(self, form, model, is_created):
        if not is_created and model.new_password:  # only check on edited new_password
            model.password = model.new_password

    def __init__(self, **kwargs):
        super(UserView, self).__init__(User, **kwargs)


class PasswordHistoryView(BaseModelView):
    can_create = False
    can_edit = False

    column_exclude_list = ('password')
    column_labels = dict(created_at='Created at')
    column_descriptions = dict(created_at='UTC time')
    column_formatters = dict(created_at=lambda v, c, m, p: m.created_at.strftime('%Y-%m-%d %H:%M:%S'))

    def __init__(self, **kwargs):
        super(PasswordHistoryView, self).__init__(PasswordHistory, **kwargs)


class RoleView(BaseModelView):
    column_display_all_relations = True

    def __init__(self, **kwargs):
        super(RoleView, self).__init__(Role, **kwargs)


class UserRolesView(BaseModelView):
    can_create = False
    can_edit = False

    def __init__(self, **kwargs):
        super(UserRolesView, self).__init__(UserRoles, **kwargs)


admin_gui = Admin()

admin_gui.add_view(UserView())
admin_gui.add_view(PasswordHistoryView())
admin_gui.add_view(RoleView())
admin_gui.add_view(UserRolesView())
