#!/usr/bin/env python

import base64
from datetime import timedelta
import json
import time

from app.models import User, PasswordHistory
from tests import update_passwords
from tests.base import BaseTest


class UserApiTest(BaseTest):
    def setUp(self):
        super(UserApiTest, self).setUp()
        self.api_url_prefix = self.app.config['API_URL_PREFIX']
        self.auth_endpoint = self.app.config['JWT_AUTH_URL_RULE']
        self.auth_header_prefix = self.app.config['JWT_AUTH_HEADER_PREFIX']

    def _auth(self, username=None, password=None):
        res = self._post(self.auth_endpoint, data=json.dumps(
            {'username': username, 'password': password}))
        return res

    def _update_password(self, new_password=None):
        res = self._post(self.api_url_prefix + '/user/update_password',
                         data=json.dumps({'password': new_password}))
        return res

    def _post(self, endpoint, data=None, headers=None):
        if hasattr(self, 'token'):
            headers = {
                'Authorization': self.auth_header_prefix + ' ' + self.token
            }
        return self.client.post(endpoint, data=data,
                                headers=headers,
                                content_type='application/json')

    def test_auth_invalid_request(self):
        auth_resp = self._auth('not')
        self.assert_401(auth_resp)
        self.assertTrue(auth_resp.json['message'] == 'Invalid credentials')
    
    def test_auth_invalid_credentials(self):
        auth_resp = self._auth('not', 'existing')
        self.assert_401(auth_resp)
        self.assertTrue(auth_resp.json['message'] == 'Invalid credentials')
    
    def test_auth_account_got_locked(self):
        auth_resp = self._auth('jane', 'roflmao')
        self.assert_403(auth_resp)
        self.assertTrue(auth_resp.json['message'] == 'Account got locked')
    
    def test_auth_account_is_locked(self):
        auth_resp = self._auth('john', '8@LbCxtLk0M6FY7k')
        self.assert_403(auth_resp)
        self.assertTrue(auth_resp.json['message'] == 'Account is locked')
    
    def test_auth_password_expired(self):
        u = User.query.filter_by(username='mikkel').scalar()
        u.password_timestamp -= self.app.config['USER_PASSWORD_RESET_DELTA']
    
        auth_resp = self._auth('mikkel', 'p6&zBk9YbBcv%7zr')
        self.assert_status(auth_resp, 209)
        self.assertTrue(auth_resp.json['message'] == 'Password has expired')
        self.assertIn('token', auth_resp.json)
    
    def test_auth_password_reset_forced(self):
        auth_resp = self._auth('jane', 'GK4Po^iMjrfjXm!9')
        self.assert_status(auth_resp, 209)
        self.assertTrue(auth_resp.json['message'] == 'Password reset forced')
        self.assertIn('token', auth_resp.json)
    
    def test_auth_valid_credentials(self):
        auth_resp = self._auth('mikkel', 'p6&zBk9YbBcv%7zr')
        self.assert_200(auth_resp)
        self.assertIn('token', auth_resp.json)
        self.token = auth_resp.json['token']
    
        # check login attempts is reset
        u = User.query.filter_by(username='mikkel').scalar()
        self.assertTrue(u.login_attempts == 0)
    
        # check contents of payload
        payload_part = str(self.token.split('.')[1])
        payload = json.loads(base64.urlsafe_b64decode(payload_part + '=' * (4 - len(payload_part) % 4)))
        self.assertIn('iat', payload)
        self.assertIn('exp', payload)
        self.assertIn('nbf', payload)
        self.assertTrue(payload['id'] == 1)
        self.assertTrue(all(role in ['user-service-api', 'admin', 'cc', 'portal'] for role in payload['aud']))
    
    def test_update_password_missing_auth_header(self):
        update_pw_resp = self._update_password()
        self.assert_401(update_pw_resp)
    
    def test_update_password_invalid_auth_token(self):
        self.token = 'loltoken'
        update_pw_resp = self._update_password()
        self.assert_401(update_pw_resp)
    
    def test_update_password_expired_token(self):
        self.app.config['JWT_EXPIRATION_DELTA'] = timedelta(seconds=0)
        self.app.config['JWT_LEEWAY'] = timedelta(seconds=0)
    
        auth_resp = self._auth('mikkel', 'p6&zBk9YbBcv%7zr')
        self.token = auth_resp.json['token']
    
        time.sleep(1)
    
        update_pw_resp = self._update_password()
        self.assert_401(update_pw_resp)
    
    def test_update_password_invalidated_token(self):
        auth_resp = self._auth('mikkel', 'p6&zBk9YbBcv%7zr')
        self.token = auth_resp.json['token']
    
        time.sleep(1)
    
        u = User.query.filter_by(username='mikkel').scalar()
        u.update_password('$oFhV6bXh%dPx5EL')
    
        update_pw_resp = self._update_password()
        self.assert_401(update_pw_resp)
    
    def test_update_password_weak(self):
        auth_resp = self._auth('jane', 'GK4Po^iMjrfjXm!9')
        self.token = auth_resp.json['token']
    
        update_pw_resp = self._update_password('soWEAK123')
        self.assert_status(update_pw_resp, 406)
        self.assertTrue(update_pw_resp.json['message'] == 'Password must have: 1 symbol or more')

    def test_update_password_in_history(self):
        update_passwords()

        auth_resp = self._auth('mikkel', 'BU!2N^$lF*Qn@H%r')
        self.token = auth_resp.json['token']

        update_pw_resp = self._update_password('$oFhV6bXh%dPx5EL')
        self.assert_status(update_pw_resp, 406)
        self.assertTrue(update_pw_resp.json['message'] == 'Password is in the list of the last %s used passwords'
                        % self.app.config['USER_PASSWORD_HISTORY'])

    def test_update_password_valid_token(self):
        auth_resp = self._auth('jane', 'GK4Po^iMjrfjXm!9')
        self.token = auth_resp.json['token']
    
        update_pw_resp = self._update_password('$oFhV6bXh%dPx5EL')
        self.assert_200(update_pw_resp)
    
        # check password reset is set to False
        u = User.query.filter_by(username='jane').scalar()
        self.assertFalse(u.password_reset)
