#!/usr/bin/env python

from flask import Flask
from sqlalchemy import event

from .admin_gui import admin_gui
from .database import db
from .jwt import jwt
from .models import User
from .user_api import user_api
from .utils import on_user_password_set, set_up_production_logging


def create_app(config=None):
    app = Flask(__name__)
    app.config.from_object('config.' + config)

    db.init_app(app)
    admin_gui.init_app(app)
    jwt.init_app(app)

    # user api
    app.register_blueprint(user_api, url_prefix=app.config['API_URL_PREFIX'])

    # listen for changes to an user's password
    event.listen(User.password, 'set', on_user_password_set)

    # set up logging
    with app.app_context():
        set_up_production_logging()

    return app
