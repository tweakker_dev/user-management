#!/usr/bin/env python

from datetime import timedelta


class Config(object):
    DEBUG = False
    TESTING = False

    SECRET_KEY = '\x99\xae\xbe\xc3\x1bs\x8b\x1d\x14\x84\xb3\xc7)h\xea\xcbI\x93cEQT\x9b\xb5'

    SQLALCHEMY_TRACK_MODIFICATIONS = False

    API_URL_PREFIX = '/api/v1'

    JWT_AUTH_URL_RULE = API_URL_PREFIX + '/user/auth'
    JWT_EXPIRATION_DELTA = timedelta(days=30)
    JWT_VERIFY_CLAIMS = ['signature', 'iat', 'nbf', 'exp', 'aud']
    JWT_REQUIRED_CLAIMS = ['iat', 'nbf', 'exp', 'aud']
    JWT_AUDIENCE = 'user-service-api'

    # User settings
    USER_LOGIN_ATTEMPTS_LIMIT = 5

    # Password settings
    USER_PASSWORD_MINIMUM_LENGTH = 8
    USER_PASSWORD_LOWERCASE_REQUIRED = True
    USER_PASSWORD_UPPERCASE_REQUIRED = True
    USER_PASSWORD_DIGIT_REQUIRED = True
    USER_PASSWORD_SYMBOL_REQUIRED = True
    USER_PASSWORD_RESET_DELTA = timedelta(days=90)
    USER_PASSWORD_HISTORY = 6


class ProductionConfig(Config):
    SERVER = 'dba-ser'
    PORT = '3306'
    DATABASE = 'user_service'
    USERNAME = 'usr_ser_write'
    PASSWORD = 'dfs%UIMyWHV&&9%R'

    SQLALCHEMY_DATABASE_URI = 'mysql://%s:%s@%s:%s/%s' % (USERNAME, PASSWORD, SERVER, PORT, DATABASE)

    LOG_DIRECTORY = '/var/log/mobilethink'
    LOG_FILE_NAME = 'mt-user-service-sterr.log'


class DevelopmentConfig(Config):
    DEBUG = True

    SERVER = 'uat-dba-srv-01'
    PORT = '3306'
    DATABASE = 'user_service'
    USERNAME = 'usr_ser_write'
    PASSWORD = 'dfs%UIMyWHV&&9%R'

    SQLALCHEMY_DATABASE_URI = 'mysql://%s:%s@%s:%s/%s' % (USERNAME, PASSWORD, SERVER, PORT, DATABASE)


class TestingConfig(Config):
    TESTING = True

    SQLALCHEMY_DATABASE_URI = 'sqlite://'
