#!/usr/bin/env python

from datetime import datetime
from flask import current_app
from flask.ext.bcrypt import check_password_hash
import logging
import re
import os

from .database import db
from .models import PasswordHistory


def on_user_password_set(target, value, oldvalue, initiator):

    # validate password on create and edit
    password_validation = password_check(value.secret)
    assert password_validation['password_ok'], password_error_generator(password_validation)

    # check if an edit has updated the password
    if target.id and oldvalue != value.secret:

        # get X last used passwords
        used_passwords = PasswordHistory.query.filter_by(user=target).order_by(
            PasswordHistory.created_at.desc()
        ).limit(current_app.config['USER_PASSWORD_HISTORY']).all()

        # check that the new password is not in the list
        for used_password in used_passwords:
            used_before = check_password_hash(used_password.password, str(value.secret))
            assert not used_before, 'Password is in the list of the last %s used passwords' \
                                    % current_app.config['USER_PASSWORD_HISTORY']

        # move current password and password timestamp to history table
        pw_history = PasswordHistory(user=target, password=oldvalue.hash, created_at=target.password_timestamp)
        db.session.add(pw_history)
        db.session.commit()

        # update password timestamp
        target.password_timestamp = datetime.utcnow()

        # disable forced password reset (if applicable)
        target.password_reset = False


def password_check(password):
    """
    Verify the strength of 'password'
    Returns a dict indicating the wrong criteria
    A password is considered strong if:
        X characters or more
        1 lowercase letter or more (if applicable)
        1 uppercase letter or more (if applicable)
        1 digit or more (if applicable)
        1 symbol or more (if applicable)
    """

    # calculating the length
    length_error = len(password) < current_app.config['USER_PASSWORD_MINIMUM_LENGTH']

    # searching for lowercase (if applicable)
    lowercase_error = re.search(r'[a-z]', password) is None \
        if current_app.config['USER_PASSWORD_LOWERCASE_REQUIRED'] else False

    # searching for uppercase (if applicable)
    uppercase_error = re.search(r'[A-Z]', password) is None \
        if current_app.config['USER_PASSWORD_UPPERCASE_REQUIRED'] else False

    # searching for digits (if applicable)
    digit_error = re.search(r'\d', password) is None \
        if current_app.config['USER_PASSWORD_DIGIT_REQUIRED'] else False

    # searching for symbols (if applicable)
    symbol_error = re.search(r'[!$%@#]', password) is None \
        if current_app.config['USER_PASSWORD_SYMBOL_REQUIRED'] else False

    # overall result
    password_ok = not (length_error or lowercase_error or uppercase_error or digit_error or symbol_error)

    return {
        'password_ok': password_ok,
        'length_error': length_error,
        'lowercase_error': lowercase_error,
        'uppercase_error': uppercase_error,
        'digit_error': digit_error,
        'symbol_error': symbol_error,
    }


def password_error_generator(password_check_dict):
    error_strings = []

    if password_check_dict['length_error']:
        error_strings.append('%s characters or more' % current_app.config['USER_PASSWORD_MINIMUM_LENGTH'])
    if password_check_dict['lowercase_error']:
        error_strings.append('1 lowercase letter or more')
    if password_check_dict['uppercase_error']:
        error_strings.append('1 uppercase letter or more')
    if password_check_dict['digit_error']:
        error_strings.append('1 digit or more')
    if password_check_dict['symbol_error']:
        error_strings.append('1 symbol or more')

    return 'Password must have: ' + format_list(error_strings)


def format_list(elements):
    n = len(elements)
    if n > 1:
        return ('{}, ' * (n - 2) + '{} & {}').format(* elements)
    elif n > 0:
        return elements[0]
    else:
        return ''


def set_up_production_logging():
    if not current_app.debug and not current_app.testing:

        if not os.path.exists(current_app.config['LOG_DIRECTORY']):
            os.makedirs(current_app.config['LOG_DIRECTORY'])

        file_handler = logging.FileHandler(
            os.path.join(
                current_app.config['LOG_DIRECTORY'],
                current_app.config['LOG_FILE_NAME']
            )
        )
        file_handler.setLevel(logging.DEBUG)
        file_handler.setFormatter(logging.Formatter(
            '%(asctime)s %(levelname)s: %(message)s [in %(pathname)s:%(lineno)d]'
        ))

        root_logger = logging.getLogger()
        root_logger.addHandler(file_handler)
