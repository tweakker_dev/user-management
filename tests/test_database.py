#!/usr/bin/env python

from flask.ext.bcrypt import check_password_hash

from app.models import User, PasswordHistory
from tests import update_passwords
from tests.base import BaseTest


class ModelsTest(BaseTest):
    def setUp(self):
        super(ModelsTest, self).setUp()
        update_passwords()

    def test_insert_user_and_encrypted_password_and_password_updates(self):
        user = User.query.filter_by(username='mikkel').scalar()
        pw_history = PasswordHistory.query.filter_by(user=user).all()

        pw_0 = 'p6&zBk9YbBcv%7zr'
        pw_1 = '$oFhV6bXh%dPx5EL'
        pw_2 = 'pQ2P&insBLvu*%bE'
        pw_3 = 'BU!2N^$lF*Qn@H%r'

        # check user attributes
        self.assertTrue(hasattr(user, 'id'))
        self.assertTrue(all(role in ['admin', 'cc', 'portal'] for role in user.get_role_names()))
        self.assertFalse(user.password_reset)
        self.assertTrue(user.login_attempts == 1)
        self.assertTrue(user.updated_at >= user.password_timestamp >= user.created_at)

        # check current password (using native equality here because it's a PasswordType from SQLAlchemy)
        self.assertTrue(user.password == pw_3)

        # check old passwords (using bcrypt here because it's a string)
        self.assertTrue(check_password_hash(pw_history[0].password, pw_0))
        self.assertTrue(check_password_hash(pw_history[1].password, pw_1))
        self.assertTrue(check_password_hash(pw_history[2].password, pw_2))

        # check password timestamps
        self.assertTrue(user.password_timestamp >= pw_history[2].created_at >= pw_history[1].created_at >= pw_history[0].created_at)
