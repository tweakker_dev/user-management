#!/usr/bin/env python

from app.database import db
from app.models import User, Role


def add_test_data():
    admin = Role(name='admin')
    cc = Role(name='cc')
    portal = Role(name='portal')
    db.session.add_all([admin, cc, portal])

    mikkel = User(username='mikkel', password='p6&zBk9YbBcv%7zr', roles=[admin, cc, portal], login_attempts=1, password_reset=False)
    john = User(username='john', password='8@LbCxtLk0M6FY7k', roles=[portal], login_attempts=5, password_reset=False)
    jane = User(username='jane', password='GK4Po^iMjrfjXm!9', login_attempts=4)
    db.session.add_all([mikkel, john, jane])

    db.session.commit()


def update_passwords():
    mikkel = User.query.filter_by(username='mikkel').scalar()
    mikkel.password = '$oFhV6bXh%dPx5EL'
    db.session.commit()

    mikkel = User.query.filter_by(username='mikkel').scalar()
    mikkel.password = 'pQ2P&insBLvu*%bE'
    db.session.commit()

    mikkel = User.query.filter_by(username='mikkel').scalar()
    mikkel.password = 'BU!2N^$lF*Qn@H%r'
    db.session.commit()
