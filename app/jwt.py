#!/usr/bin/env python

import logging
from datetime import datetime

from flask import current_app, jsonify
from flask.ext.jwt import JWT, JWTError

from .models import User

logger = logging.getLogger(__name__)

jwt = JWT()


@jwt.authentication_handler
def authenticate(username, password):
    user = User.query.filter(User.username == username).scalar()
    if user:
        if user.login_attempts >= current_app.config.get('USER_LOGIN_ATTEMPTS_LIMIT'):
            raise JWTError('Account locked', 'Account is locked', status_code=403)

        if user.password == password:
            user.reset_login_attempts()
            return user
        else:
            user.add_login_attempt()

            if user.login_attempts >= current_app.config.get('USER_LOGIN_ATTEMPTS_LIMIT'):
                raise JWTError('Account locked', 'Account got locked', status_code=403)


@jwt.identity_handler
def user_handler(payload):
    if 'id' in payload and 'iat' in payload:
        user = User.query.filter(User.id == payload['id']).scalar()

        # check if password has been changed since the token was issued
        if user and datetime.utcfromtimestamp(payload['iat']) >= user.password_timestamp.replace(microsecond=0):
            return user


@jwt.jwt_payload_handler
def make_payload(user):
    iat = datetime.utcnow()
    exp = iat + current_app.config.get('JWT_EXPIRATION_DELTA')
    nbf = iat + current_app.config.get('JWT_NOT_BEFORE_DELTA')
    audience = [current_app.config.get('JWT_AUDIENCE')] + user.get_role_names()
    user_id = user.id
    return {'exp': exp, 'iat': iat, 'nbf': nbf, 'id': user_id, 'aud': audience}


@jwt.auth_response_handler
def auth_response_handler(token, user):
    response = {'token': token.decode('utf-8')}
    status_code = 200

    if user.password_reset:
        response['message'] = 'Password reset forced'
        status_code = 209

    if user.password_timestamp <= (datetime.utcnow() - current_app.config.get('USER_PASSWORD_RESET_DELTA')):
        response['message'] = 'Password has expired'
        status_code = 209

    return jsonify(response), status_code


@jwt.jwt_error_handler
def error_handler(error):
    if not current_app.debug and not current_app.testing:
        logger.error(error)
    return jsonify({'message': error.description}), error.status_code, error.headers
